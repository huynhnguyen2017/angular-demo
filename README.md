# Run application on local

ng serve

## Dockerize application

* npm run docker
* npm run ng-deployment

## Document for build, run, deploy
1. Execute commands from package.json
Link: https://gist.github.com/duluca/d13e501e870215586271b0f9ce1781ce
2. Blog for deploy on docker
Link: https://developer.okta.com/blog/2020/06/17/angular-docker-spring-boot#create-a-docker-container-for-your-angular-app
